const express = require('express');

const cors = require('cors');
require('dotenv').config();
const secretKey = process.env.SECRET_KEY;
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
const app = express();

app.use(express.json());
app.use(cors());

let clientSecret;

app.post('/checkout', async (req, res) => {
    try {
        // Generate client secret only if not already generated
        if (!clientSecret) {
            const paymentIntent = await stripe.paymentIntents.create({
                amount: 1000, // Amount in cents
                currency: 'usd',
                metadata: { integration_check: 'accept_a_payment' },
            });
            clientSecret = paymentIntent.client_secret;
        }
        
        res.json({ clientSecret }); // Return the same client secret
    } catch (error) {
        console.error('Error creating PaymentIntent:', error);
        res.status(500).json({ error: 'Internal server error' });
    }
});

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
    console.log(process.env.STRIPE_SECRET_KEY)
});
