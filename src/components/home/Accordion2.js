import React from 'react';
import AccordionItem2 from './AccordionItem2';

const Accordion2 = () => {
    return (
        <div className='max-w-[1160px] mx-auto py-3 px-3'>
            <AccordionItem2 title="Do I get lifetime access?">
                Yes, you get lifetime access with a login and PDF download link of the 16,000+ ChatGPT Prompt sent to you via email right after completing your order.
            </AccordionItem2>
            <AccordionItem2 title="Is there a guarantee?">
                YES! If for any reason you want your money back, just send us a message and we'll give you 100% of your money back...PLUS we'll let you keep access to your purchase. We 100% stand behind the quality of our product.
            </AccordionItem2>
            <AccordionItem2 title="Why is it ONLY $27.00?">
                The price for "The Ultimate A.I. Marketing Toolkits" is normally $97.00... BUT as part of a pre-launch marketing campaign to gather testimonials, we're giving it away for the special price of just $27.00! Naturally we can't afford to advertise it at this price for long, so grab it now while you still can!
            </AccordionItem2>
            <AccordionItem2 title="Why do I need prompts when I can come up with ideas myself?">
                ChatGPT uses a language model that relies on very precise phrases to deliver quality results. This means that one single word can ruin results, or really boost them.Using our prompts, you’ll be able to save time and unlock features of ChatGPT you never thought possible (while your competition is still trying to manually type all their questions).
            </AccordionItem2>
            <AccordionItem2 title="Who is this prompt pack for?">
                We designed this pack to serve the specific needs of coaches, agency owners, freelancers, and online entrepreneurs.
            </AccordionItem2>
            <AccordionItem2 title="How is this different from other prompt packs or YouTube videos out there?">
                The reason why you’re reading this is that you likely don’t have much free time. So why not get all the condensed, researched prompts that are proven to work?We haven’t found any ChatGPT Prompt Pack that includes the vast level of detail and the number of tasks that we included. It has everything a digital marketer needs to thrive, plus lifetime updates to never fall behind.
            </AccordionItem2>
            <AccordionItem2 title="Will this help me with other platforms like Bard or ChatSonic?">
                Sure! ChatGPT is only the most viral AI right now, but these prompts are equally applicable to any other AI that uses a dialogue model to operate. Microsoft will soon embed to Bing, their search engine, and you can sure use our prompt pack there!
            </AccordionItem2>
            <AccordionItem2 title="I've still got questions.">
                Sure! Just send us an email at <span className='text-[#4f8ff7]'> support@aitoolkitsforcoaches.com </span>and we will take care of you.
            </AccordionItem2>

        </div>
    );
};

export default Accordion2;