import React, { useState, useEffect } from 'react';
import { useStripe, useElements, ExpressCheckoutElement } from '@stripe/react-stripe-js';

export const CheckoutPage = () => {
  const stripe = useStripe();
  const elements = useElements();
  const [errorMessage, setErrorMessage] = useState('');
  const [clientSecret, setClientSecret] = useState('');

  useEffect(() => {
    const fetchClientSecret = async () => {
      try {
        const response = await fetch('https://35b6-197-156-107-201.ngrok-free.app/checkout', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
        });
        const data = await response.json();
        console.log('Client secret fetched:', data.clientSecret);
        setClientSecret(data.clientSecret);
      } catch (error) {
        console.error('Error fetching client secret:', error);
        setErrorMessage('Error fetching client secret. Please try again.');
      }
    };

    fetchClientSecret();
  }, []);

  useEffect(() => {
    console.log('Client secret in state:', clientSecret);
  }, [clientSecret]);

  const onConfirm = async () => {
    if (!stripe || !elements || !clientSecret) {
      return;
    }

    try {
      const result = await stripe.confirmCardPayment(clientSecret, {
        payment_method: {
          card: elements.getElement(ExpressCheckoutElement),
          billing_details: {},
        },
      });

      if (result.error) {
        setErrorMessage(result.error.message);
      } else {
        // Handle success case
      }
    } catch (error) {
      console.error('Error confirming payment:', error);
      setErrorMessage('Error confirming payment. Please try again.');
    }
  };

  return (
    <div id="checkout-page">
      {clientSecret && (
        <ExpressCheckoutElement
          clientSecret={clientSecret}
          onConfirm={onConfirm}
        />
      )}
      {errorMessage && <div>{errorMessage}</div>}
    </div>
  );
};
