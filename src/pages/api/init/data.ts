//@ts-nocheck

import { NextApiRequest, NextApiResponse } from 'next'
import Stripe from 'stripe';

const stripeClient = new Stripe(process.env.REACT_APP_STRIPE_SECRET_KEY!, {
  // https://github.com/stripe/stripe-node#configuration
  apiVersion: '2024-03-12',
})

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  if (req.method !== 'POST') {
    res.setHeader('Allow', 'POST');
    return res.status(405).end('Method Not Allowed');
  }

  const { amount, currency } = req.body;

  try {
    // Create Payment Intent
    const paymentIntent = await stripeClient.paymentIntents.create({
      amount,
      currency,
      // Add any other necessary parameters
    });

    // Obtain Client Secret from Payment Intent
    const clientSecret = paymentIntent.client_secret;

    // Send Client Secret to Client
    res.status(200).json({ clientSecret });
  } catch (error) {
    console.error('Error creating payment intent:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
}
